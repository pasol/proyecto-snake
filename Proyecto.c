#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>

#define FRAMES 16
#define VELOCIDAD 100


//Funcion ESCAPE
#define ESC 27

//CARACTERES PARA EL ESCENARIO
#define ESQ_SUP_IZQ 218
#define ESQ_SUP_DER 191
#define ESQ_INF_IZQ 192
#define ESQ_INF_DER 217
#define LINEA_HOR 196
#define LINEA_VER 124

//MEDIDAS DEL ESCENARIO
 #define LX 50
 #define LY 50
//-----------------------
 #define XMARCO 5
 #define YMARCO 5

//CARACTERES QUE USARA LA SERPIENTE
#define CUERPO 'O'
#define CABEZA 148

//TECLAS PARA MOVERSE
#define ARRIBA 72
#define ABAJO 80
#define IZQUIERDA 75
#define DERECHA 77

//VALORES DE FRUIT
#define FRUIT 1
#define FRUIT_NA 5
#define PROBABILIDAD 25

//ESTRUCTURAS UTILIZADAS
struct serpiente{
    int X, Y, DG, COLOR;
    struct serpiente *nxt;
};
struct fruta{
    int X, Y, DG, COLOR, VALOR;
};

//LUBRERIAS DEL ESCENARIO
void gotoxy(int x, int y);
void ESCENARIO();

//LIBRERIAS DE LA SERPIENTE
void PARTES();
void CRECIMIENTO(int x, int y, struct serpiente *cuerp);
void Snake(struct serpiente *cuerp);
void MOVIMIENTO(int x, int y, struct serpiente *cuerp);
int colisciones_escenario();
int colisciones_snk();

//LIBRERIAS DE LA FRUTA
void Fruta();
void reposicion_frt();
void PARTES_FRT();
int colisciones_frt(struct serpiente *cuerp);

//DECLARACIONES STRUCT Y VECTORES DE ORIENTACION
struct serpiente *SERPIENTE;
struct fruta *FRUTA;
int ori[2] = {0, -1};
int cola[2];

int main(){
    int time = 0;
    int perder = 0;
    int puntaje = 0;
    int longitud = 0;
    int comer = 0;
    int moves = 0;
    char funcion = 0;

    system("cls");
    ESCENARIO();
    PARTES();
    PARTES_FRT();
    Snake(SERPIENTE);
    Fruta();
    //Bucle de juego
    while(!perder){
        if(kbhit()){
            funcion = getch();
            perder = (funcion == ESC);
            ori[0] = (funcion == DERECHA)-(funcion == IZQUIERDA);
            ori[1] = (funcion == ABAJO)-(funcion == ARRIBA);
        }
        Sleep(FRAMES);
        time+=FRAMES;
        if(time>=VELOCIDAD){
            Snake(SERPIENTE);
            Fruta();
            gotoxy(XMARCO,YMARCO-1);
            printf("PUNTUACION: %d", puntaje);
            MOVIMIENTO(SERPIENTE->X+ori[0], SERPIENTE->Y+ori[1], SERPIENTE);
            if(!comer){
                if(colisciones_frt(SERPIENTE)){
                    puntaje+=FRUTA->VALOR;
                    comer = 1;
                    moves += longitud;
                }
            }else{
                if(longitud < puntaje){
                    if(!moves){
                        CRECIMIENTO(FRUTA->X, FRUTA->Y, SERPIENTE);
                        ++longitud;
                    }
                    else{
                        --moves;
                    }
                }
                else{
                    reposicion_frt();
                    comer = 0;
                }
            }
            perder = colisciones_escenario() + colisciones_snk(SERPIENTE->nxt);
            time = 0;
        }
    }
return 0;
}


//FUNCIONES PARA EL ESCENARIO

void gotoxy(int x, int y){
    printf("%c[%d;%dH", ESC, y, x);
}
void ESCENARIO(){
    int i, j;
    char c;
    gotoxy(XMARCO, YMARCO);
    for(j=0; j<LY+2; ++j){
        for(i=0; i<LX+2; ++i){
            //Columas
            if(i==0 || i==LX+1){
                switch(j){
                    case 0: c= ESQ_SUP_IZQ*(i==0) + ESQ_SUP_DER*(i==LX+1);
                    break;                                                     //Imprime la primera columna del tablero
                    case LY+1: c= ESQ_INF_IZQ*(i==0) + ESQ_INF_DER*(i==LX+1);
                    break;                                                     //Imprime la ultima columna del tablero
                    default: c= LINEA_VER;
                    break;
                }
                //En medio
                }else{
                    if(j==0 || j==LY+1){
                        c= LINEA_HOR;
                    }else{
                        c=' ';
                    }
                }
                printf("%c",c);
                if(i==LX+1){
                    gotoxy(XMARCO, YMARCO+j+1);
                }
            }
    }
}

//FUNCIONES ESTRUCTURAS PARA LA SERPIENTE

void Snake(struct serpiente *cuerp){
    printf("%c[1;%dm", ESC, cuerp->COLOR);
    gotoxy(cuerp->X, cuerp->Y);
    printf("%c", cuerp->DG);
    printf("%c[0m", ESC);
    if(cuerp->nxt != NULL){
        Snake(cuerp->nxt);
    }
    else{
        gotoxy(cola[0], cola[1]);
        printf(" ");
    }
}

void CRECIMIENTO(int x, int y, struct serpiente *cuerp){
    if(cuerp->nxt == NULL){
        cuerp->nxt = (struct serpiente*)malloc(sizeof(struct serpiente));
        cuerp->nxt->X = x;
        cuerp->nxt->Y = y;
        cuerp->nxt->DG = CUERPO;
        cuerp->nxt->COLOR = 34;
        cuerp->nxt->nxt = NULL;
    }else{
        CRECIMIENTO(x, y, cuerp->nxt);
    }
}

void PARTES(){
    SERPIENTE = (struct serpiente*)malloc(sizeof(struct serpiente)); //Reservar memoria para la serpiente
    SERPIENTE->X = XMARCO + LX/4.5; //Coordenadas de donde va a iniciar la serpiente
    SERPIENTE->Y = YMARCO + LY/4.5;
    SERPIENTE->DG = CABEZA;
    SERPIENTE->COLOR = 31;
    SERPIENTE->nxt = NULL;
    CRECIMIENTO(XMARCO + LX/4.5, YMARCO + LY/4.5 + 1, SERPIENTE);
}

void MOVIMIENTO(int x, int y, struct serpiente *cuerp){
    if(cuerp->nxt != NULL){
        MOVIMIENTO(cuerp->X, cuerp->Y, cuerp->nxt);
    }
    else{
        cola[0] = cuerp->X;
        cola[1] = cuerp->Y;
    }
    cuerp->X=x;
    cuerp->Y=y;
}

int colisciones_escenario(){
    int A = 0;
    A = (SERPIENTE->X <= XMARCO) + (SERPIENTE->X > XMARCO + LX);
    A += (SERPIENTE->Y <= YMARCO) + (SERPIENTE->Y > YMARCO + LY);
    return A;
}

int colisciones_snk(struct serpiente *cuerp){
    static int colision;
    if((SERPIENTE->X == cuerp->X) && (SERPIENTE->Y == cuerp->Y)){
        colision = 1;
    }
    else{
        if(cuerp->nxt != NULL){
            colisciones_snk(cuerp->nxt);
        }
    }
    return colision;
}

//FUNCIONES ESTRUCTURAS PARA LA FRUTA
int colisciones_frt(struct serpiente *cuerp){
    static int choque;
    if(cuerp == SERPIENTE){
        choque = 0;
    }
    choque += (cuerp->X == FRUTA->X)&&(cuerp->Y == FRUTA->Y);
    if(!choque){
        if(cuerp->nxt != NULL){
                colisciones_frt(cuerp->nxt);
        }
    }
    return choque;
}
void reposicion_frt(){
    FRUTA->X = XMARCO + rand()%LX + 1;
    FRUTA->Y = YMARCO + rand()%LY + 1;
    if(colisciones_frt(SERPIENTE)){
        reposicion_frt();
    }
    FRUTA->VALOR = FRUIT + (FRUIT_NA-FRUIT)*(PROBABILIDAD >= rand()%101);
    FRUTA->COLOR = 32*(FRUTA->VALOR == FRUIT)+33*(FRUTA->VALOR == FRUIT_NA);
}

void PARTES_FRT(){
    FRUTA = (struct fruta*)malloc(sizeof(struct fruta));
    FRUTA->DG = CUERPO;
    reposicion_frt();

}

void Fruta(){
    printf("%c[1;%dm", ESC, FRUTA->COLOR);
    gotoxy(FRUTA->X, FRUTA->Y);
    printf("%c", FRUTA->DG);
    printf("%c[0m", ESC);
}
